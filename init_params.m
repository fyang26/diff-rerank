function param = init_params()
%% parameter setting

% dataset name
param.dataset = 'holidays';

% names of similarity/distance matrices
% similarity matrix for BoW. distance matrices for other features
param.pos_name = {'aff_bow', 'dist_vlad', 'dist_gist', 'dist_color'};
param.neg_name = {'aff_bow_neg', 'dist_vlad_neg', 'dist_gist_neg', 'dist_color_neg'};

% name of features
param.t_feat = {'t_bow', 't_vlad', 't_gist', 't_color'};

% number of images 
param.img_num = 1491;

% gaussian kernel parameters to convert distance to similarity
% they are for bow, vlad, color and gist, respectively, in order
param.sigma = [0, .5, .34, .14];

% whether to use query specific weight (qsw), or diffusion process (dp)
param.qsw = 1;
param.dp = 1;

%% number of top k and short list
param.k = [6 700];
