% This is an implementation for the reranking algorithm proposed in 
%
% Re-ranking by Multi-feature Fusion with Diffusion for Image Retrieval
% Fan Yang, Bogdan Matei and Larry S. Davis
% IEEE Winter Conference on Applications of Computer Vision (WACV), 2015. 
%
% Any questions please contact Fan Yang (fyang@umiacs.umd.edu)


close all;

%% prepare parameters
param = init_params;
feat_num = length(param.t_feat);
k = param.k(1);
sl = param.k(2);

%% compute weights 
for feat = 1:feat_num
    load(['./saved_data/' param.dataset '.mat'], param.pos_name{feat});
    load(['./saved_data/' param.dataset '_negative_neg.mat'], param.neg_name{feat});
    if feat == 1
        dist = aff_bow;
        distn = aff_bow_neg;
    else
        eval(['dist = exp(-', param.pos_name{feat}, '/', num2str(param.sigma(feat)), ');']);
        eval(['distn = exp(-', param.neg_name{feat}, '/', num2str(param.sigma(feat)), ');']);
        distn(isnan(distn)) = [];
    end
    clear(param.pos_name{feat});
    clear(param.neg_name{feat});
    vol(feat) = sum(sum(dist));

    neg_mean{feat} = mean((distn(:)));
    [~, ind{feat}] = sort(dist, 1, 'descend');
    pos{feat} = [];
    num_cnt = max(10, k+1);
    for n = 1:size(dist,2)
        pos{feat} = [pos{feat} dist(ind{feat}(2:num_cnt, n), n)];
    end
    pos_mean{feat} = mean(pos{feat}(:));
    for i = 1:size(dist,2)
        dist(ind{feat}(sl+1:end, i), i) = 0;
        dist_mean{feat} = mean(dist(ind{feat}(2:k+1, i),i));
        weight(i,feat) = exp((dist_mean{feat} - neg_mean{feat})^2 - (dist_mean{feat} - pos_mean{feat})^2);
    end
end

clear distn dist
weight = weight ./ repmat(sum(weight,2), 1, feat_num);  % for query specific weight
alpha = 1/feat_num;  % for equal weight

%% compute fused matrix
for feat = 1:feat_num
    load(['./saved_data/' param.dataset '.mat'], param.pos_name{feat});
    if feat == 1
        dist = aff_bow;
    else
        eval(['dist = exp(-', param.pos_name{feat}, '/', num2str(param.sigma(feat)), ');']);
    end
    clear(param.pos_name{feat});
    
    for i = 1:size(dist,2)
        dist(ind{feat}(sl+1:end, i), i) = 0;
    end
    if exist('w', 'var')
        if param.qsw
            w = w + repmat(weight(:,feat)', size(dist,1), 1) .* (dist / vol(feat));
        else
            w = w + alpha * (dist / vol(feat));
        end
    else
        if param.qsw
            w = repmat(weight(:,1)', size(dist,1), 1) .* (dist / vol(1));
        else
            w = alpha * (dist / vol(1));
        end
    end
end


%% do diffusion 
if param.dp
    [diff, ~] = ICG_ApplyDiffusionProcess(double(w), 0, k);
    [~, idx] = sort(diff, 1, 'descend');
else
    [~, idx] = sort(w, 1, 'descend');
end
clear diff w dist weight

%% save results
if strcmp(param.dataset, 'holidays')
    load('./saved_data/holidays_img_list.mat');
    ss = './eval_ranking/ranking_holidays.dat';
    write_ranking_holidays(im, idx', ss);
else
    % put your own code here to save and evaluate reranking results 
end